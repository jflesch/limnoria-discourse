###
# Copyright (c) 2015, Moritz Lipp
# Copyright (c) 2021, Flesch Jerome
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

import json

from supybot import (
    callbacks,
    httpserver,
    ircdb,
    ircmsgs,
    log,
    world
)
try:
    from supybot.i18n import PluginInternationalization
    from supybot.i18n import internationalizeDocstring
    _ = PluginInternationalization('Disource')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    def _(x):
        return x

    def internationalizeDocstring(x):
        return x


class DiscourseHandler(object):
    def __init__(self, plugin):
        self.plugin = plugin
        self.log = log.getPluginLogger('Discourse')
        self.irc = None

    def handle_payload(self, headers, payload, irc):
        self.irc = irc

        for channel in irc.state.channels.keys():
            if 'post' in payload:
                self._handle_post(channel, payload)

    def _handle_post(self, channel, payload):
        msg = "[forum.openpaper.work] New post in thread '{}': {}".format(
            payload['post']['topic_title'],
            "https://forum.openpaper.work/t/{}/{}/{}".format(
                payload['post']['topic_slug'],
                payload['post']['topic_id'],
                payload['post']['post_number']
            )
        )
        self._send_message(channel, msg)

    def _send_message(self, channel, msg):
        announce_msg = ircmsgs.privmsg(channel, msg)
        self.irc.queueMsg(announce_msg)


class DiscourseWebHookService(httpserver.SupyHTTPServerCallback):
    name = "DiscourseWebHookService"
    defaultResponse = (
        "This plugin handles only POST request,"
        " please don't use other requests."
    )

    def __init__(self, plugin):
        self.log = log.getPluginLogger('Discourse')
        self.discourse = DiscourseHandler(plugin)
        self.plugin = plugin

    def _send_error(self, handler, message):
        self.log.error("ERROR: %s", message)
        handler.send_response(403)
        handler.send_header('Content-type', 'text/plain')
        handler.end_headers()
        handler.wfile.write(message.encode('utf-8'))

    def _send_ok(self, handler):
        handler.send_response(200)
        handler.send_header('Content-type', 'text/plain')
        handler.end_headers()
        handler.wfile.write(bytes('OK', 'utf-8'))

    def doPost(self, handler, path, form):
        headers = dict(self.headers)

        network = None
        try:
            information = path.split('/')[1:]
            network = information[0]
        except IndexError:
            self._send_error(handler, _("""Error: You need to provide the
                                        network name in the URL."""))
            return

        irc = world.getIrc(network)
        if irc is None:
            self._send_error(
                handler, (_('Error: Unknown network %r') % network)
            )
            return

        # Handle payload
        payload = None
        try:
            payload = json.JSONDecoder().decode(form.decode('utf-8'))
        except Exception as e:
            self.log.info(e)
            self._send_error(handler, _('Error: Invalid JSON data sent.'))
            return

        try:
            self.discourse.handle_payload(headers, payload, irc)
        except Exception as e:
            self.log.info(e)
            self._send_error(handler, _('Error: Invalid data sent.'))
            return

        # Return OK
        self._send_ok(handler)


class Discourse(callbacks.Plugin):
    threaded = True

    def __init__(self, irc):
        global instance

        # Store the super() information so that reloads don't fail
        self.__parent = super()
        self.__parent.__init__(irc)
        instance = self

        callback = DiscourseWebHookService(self)
        httpserver.hook('discourse', callback)

    def die(self):
        httpserver.unhook('discourse')
        self.__parent.die()

    def _check_capability(self, irc, msg):
        if ircdb.checkCapability(msg.prefix, 'admin'):
            return True
        else:
            irc.errorNoCapability('admin')
            return False


Class = Discourse


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
