import supybot
import supybot.world as world

__version__ = ""

__author__ = supybot.Author(
    'Jerome Flesch', 'jflesch', 'jflesch@openpaper.work'
)

__contributors__ = {}

__url__ = ''

from . import config
from . import plugin
from imp import reload
# In case we're being reloaded.
reload(config)
reload(plugin)
# Add more reloads here if you add third-party modules and want them to be
# reloaded when this plugin is reloaded.  Don't forget to import them as well!

if world.testing:
    from . import test

Class = plugin.Class
configure = config.configure


# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
